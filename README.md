![Universidades](http://www.madarme.co/portada-web.png)
# Título del proyecto:

#### Ejercicio de Universidades usando  la lectura de datos JSON 
***
## Índice
1. [Características](#caracter-sticas)
2. [Contenido del proyecto](#contenido-del-proyecto)
3. [Tecnologías](#tecnologías)
4. [IDE](#ide)
5. [Instalación](#instalación)
6. [Demo](#demo)
7. [Autor(es)](#autores)
8. [Institución Académica](#institución-académica)
9. [Referencias](#referencias)
***


#### Características:

  - Proyecto con lectura de datos json a través de la API fecth JavaScript
  - Carga dinámica del JSON 
  - Archivo json de ejemplo: [ver](https://programacion-web---i-sem-2019.gitlab.io/persistencia/json_web/json/universidades.json)
***
  #### Contenido del proyecto

| Archivo      | Descripción  |
|--------------|--------------|
| [index.html](https://gitlab.com/jairoguillermogure/listas_universidades_expo_readme/-/blob/main/index.html) | Archivo principal de invocación a la lectura de JSON|
| [js/proceso.js](https://gitlab.com/jairoguillermogure/listas_universidades_expo_readme/-/blob/main/js/funcion.js) | Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados|

  
***
#### Tecnologías

  - [![HTML5](https://img.shields.io/badge/HTML5-CSS-blue)](https://developer.mozilla.org/es/docs/Web/Guide/HTML/HTML5)
  - [![JavaScript](https://img.shields.io/badge/JavaScript-blue)](https://developer.mozilla.org/es/docs/Web/JavaScript)
  


Usted puede ver el siguiente marco conceptual sobre la API fetch:

  - [Vídeo explicativo lectura con fetch()](https://www.youtube.com/watch?v=DP7Hkr2ss_I)
  - [Gúia de Mozzilla JSON](https://developer.mozilla.org/es/docs/Learn/JavaScript/Objects/JSON)
  
  ***
#### IDE

- El proyecto se desarrolla usando sublime text 3, es un editor de texto para código en diferentes lenguajes, se destaca su versatilidad para el manejo de marcas y autocompletado [(Kinder, 2013)](#kinder-k-2013-sublime-text-one-editor-to-rule-them-all-linux-journal-2013232-2).
- Visor de JSON -(http://jsonviewer.stack.hu/)

***
### Instalación

Firefox Devoloper Edition-> [descargar](https://www.mozilla.org/es-ES/firefox/developer/).
El software es necesario para ver la interacción por consola y depuración del código JS


```sh
-Descargar proyecto
-Invocar página index.html desde Firefox 
```

***
### Demo

Para ver el demo de la aplicación puede dirigirse a: [Universidades](https://jairoguillermogure.gitlab.io/listas_universidades_expo_readme).

***
### Autor(es)
Proyecto desarrollado por [Jairo Gil] (<jairoguillermogure@ufps.edu.co>).


***
### Institución Académica   
Proyecto desarrollado en la Materia programación web del  [Programa de Ingeniería de Sistemas] de la [Universidad Francisco de Paula Santander]


   [Jairo Gil]: <https://web.facebook.com/jairoguillermo.gilurena.1>
   [Programa de Ingeniería de Sistemas]:<https://ingsistemas.cloud.ufps.edu.co/>
   [Universidad Francisco de Paula Santander]:<https://ww2.ufps.edu.co/>
   
   ***
### Referencias
   

###### Kinder, K. (2013). Sublime text: one editor to rule them all?. Linux Journal, 2013(232), 2.